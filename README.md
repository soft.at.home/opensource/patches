# Opensource patches

This repository contains patches for open source components. They are grouped together in a folder with the name of the open source project.

## List of patched open source components

- [mosquitto v2.0.5](https://github.com/eclipse/mosquitto/tree/v2.0.5)
